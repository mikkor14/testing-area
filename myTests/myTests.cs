﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Calculator;



namespace myTests
{
    public class myTests
    {


        [Theory]
        [InlineData(4,5,9)]
        [InlineData(23.3,25.5,48.8)]
        public void addition_ShouldFindSumOfTwoNumbers(double a, double b, double expected)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Add(a, b);

            //Assert
            Assert.Equal(expected, actual);

        }

        [Theory]
        [InlineData(4, 5, -1)]
        [InlineData(40, 25.5, 14.5)]
        public void substraction_ShouldFindSubstractionOfTwoNumbers(double a, double b, double expected)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Substract(a, b);

            //Assert
            Assert.Equal(expected, actual);

        }

        [Theory]
        [InlineData(4, 5, 20)]
        [InlineData(9.1, 4, 36.4)]
        public void multiply_ShouldFindProductOfTwoNumbers(double a, double b, double expected)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Multiply(a, b);

            //Assert
            Assert.Equal(expected, actual);

        }

        [Theory]
        [InlineData(4, 5, 0.8)]
        [InlineData(20, 4, 5)]
        public void division_ShouldFindDivisorOfTwoNumbers(double a, double b, double expected)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Divide(a, b);

            //Assert
            Assert.Equal(expected, actual);

        }
        [Theory]
        [InlineData(5,0)]
        public void division_DivideByZeroShouldBeInfinite(double a, double b)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Divide(a, b);

            //Assert
            Assert.True(Double.IsInfinity(actual));

        }

        [Theory]
        [InlineData(5)]
        [InlineData(1.333)]
        [InlineData(0)]
        public void multiply_MultiplyByZeroShouldBeZero(double a)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual = calc.Multiply(a, 0);

            //Assert
            Assert.Equal(0, actual);
        }


        [Theory]
        [InlineData(5,1,5)]
        [InlineData(1,123,123)]
        [InlineData(1.512, 1, 1.512)]
        public void multiply_MultiplyByOneShouldBeSameNumber(double a, double b, double expected)
        {

            //Arrange
            CalculatorProgram calc = new CalculatorProgram();

            //Act
            double actual1 = calc.Multiply(a, b);
            double actual2 = calc.Multiply(a, b);
            double actual3 = calc.Multiply(a, b);

            //Assert
            Assert.Equal(expected, actual1);
            Assert.Equal(expected, actual2);
            Assert.Equal(expected, actual3);
        }



    }
}
